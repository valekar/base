package com.srini.gatewayservice.facade.userservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String line1;
    private String line2;
    private String pinCode;
    private String city;
    private String state;
    private String country;
    private String county;
}

