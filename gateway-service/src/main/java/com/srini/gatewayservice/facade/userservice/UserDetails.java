package com.srini.gatewayservice.facade.userservice;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {

    private Long age;
    private Address address;
    private String sex;
    private String blood_group;
}

