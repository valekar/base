package com.srini.gatewayservice.facade.userservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGroup {
    private Long user_group_id;
    private String id;
    private String name;
    private Privilege privileges;
    private boolean active;
}

