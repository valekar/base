package com.srini.utility;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class MyMethodReflection {

    public static List<String> getClassColumns(Class aClass){
        //Class aClass = myClass;
        Field[] fields = aClass.getDeclaredFields();
        List<String> strList = new ArrayList<>();
        for(Field f : fields){
            if(Modifier.isPrivate(f.getModifiers())){
                //System.out.println(f.getName().toString());
                strList.add(f.getName());
            }
        }
        return strList;

    }
}
