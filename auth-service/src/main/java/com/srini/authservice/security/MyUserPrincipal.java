package com.srini.authservice.security;

import com.srini.authservice.model.Privilege;
import com.srini.authservice.model.User;
import com.srini.authservice.model.UserGroup;
import com.srini.utility.MyMethodReflection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyUserPrincipal implements UserDetails {

    private User user;
    public MyUserPrincipal(User user) {
        this.user = user;
    }



    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //ser.getColumnNames("privileges");
        List<String> privilegeColumns = MyMethodReflection.getClassColumns(Privilege.class);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        //for (Privilege privilege : user.getPrivileges()) {
        UserGroup userGroup = user.getUserGroup();
        Privilege privilege = userGroup.getPrivileges();
        for(String pColumn : privilegeColumns){
            try{
                Method method = new PropertyDescriptor(pColumn, Privilege.class).getReadMethod();
                if(!method.getName().endsWith("Id")){
                    String authority = pColumn.concat("-").concat(method.invoke(privilege).toString());
                    authorities.add(new SimpleGrantedAuthority(authority));
                }
            }
            catch (Exception ex){}
        }
        // }
        return authorities;
    }

    public User getUser() {
        return user;
    }
}