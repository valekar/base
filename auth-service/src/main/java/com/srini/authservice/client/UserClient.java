package com.srini.authservice.client;

import com.srini.authservice.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Primary
@FeignClient(name="user-service",fallback = UserClientFallback.class)
public interface UserClient {

    //@RequestMapping(method = RequestMethod.PUT, value = "/statistics/{accountName}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @GetMapping("/user/testMe")
    String testMe();

    @GetMapping("/user/{username}")
    User getUser(@PathVariable("username") String username);
}


@Component
class UserClientFallback implements UserClient{


    @Override
    public String testMe() {
        return "FALLL BAKCC";
    }

    @Override
    public User getUser(String username) {
        User user = new User("Dummy User");
        return user;
    }
}
