package com.srini.authservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {

    static final long serialVersionUID = 1L;

    public User(String username) {
        this.username = username;
    }

    private Long userId;
    private String username;
    private String id;
    private String email;
    private boolean active = true;
    //private UserDetails userDetails;
    private UserGroup userGroup;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;

    private boolean enabled;
    private String password;


}
