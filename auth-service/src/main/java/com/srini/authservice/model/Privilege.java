package com.srini.authservice.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Privilege {
    private boolean orders;
    private boolean inward;
    private boolean production;
    private boolean checking;
    private boolean reports;
    private boolean admin;
    private boolean store;
    private boolean company;
    private boolean preProduction;
}
