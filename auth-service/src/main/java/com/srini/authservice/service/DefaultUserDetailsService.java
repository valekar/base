package com.srini.authservice.service;


import com.srini.authservice.client.UserClient;
import com.srini.authservice.model.User;
import com.srini.authservice.security.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service("userDetailsService")
public class DefaultUserDetailsService implements UserDetailsService {


    @Autowired
    private PasswordEncoder passwordEncoder;


    private UserClient userClient;

    //@Autowired
    public DefaultUserDetailsService(UserClient userClient){
        this.userClient = userClient;
    }


	/*@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findOneByUsername(username);
	}*/

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userClient.getUser(username);
        //User user = userRepository.findOneByUsername(username);
       /* if (user == null) {
            throw new UsernameNotFoundException(username);
        }*/
        //getPrivilegeColumns();
        return new MyUserPrincipal(user);
    }




}
