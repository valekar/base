package com.srini.userservice.domain.user.facade;

import com.srini.userservice.domain.user.model.UserDetails;
import com.srini.userservice.domain.user.model.UserGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserData {

    private String id;
    private Long user_id;
    private String username;
    private String password;
    private boolean enabled;
    private String email;
    private boolean active;
    private UserDetails userDetails;
    private UserGroup userGroup;
    private String created_by;
    private  String created_user_id;
    private String updated_by;
    private String updated_user_id;
}
