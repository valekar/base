package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.commands.UserGroupCreateCmd;
import com.srini.userservice.domain.user.commands.UserGroupDeleteCmd;
import com.srini.userservice.domain.user.commands.UserGroupUpdateCmd;
import com.srini.userservice.domain.user.events.UserGroupCreatedEvent;
import com.srini.userservice.domain.user.events.UserGroupDeletedEvent;
import com.srini.userservice.domain.user.events.UserGroupUpdatedEvent;
import com.srini.userservice.domain.user.model.Privilege;
import com.srini.userservice.domain.user.model.UserGroup;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.modelmapper.ModelMapper;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
public class UserGroupAggregate {

    @AggregateIdentifier
    private String id;
    private Privilege privileges;
    private UserGroup userGroup;
    private boolean isActive;
    private String loggedInUser;

    public UserGroupAggregate() {
    }

    @CommandHandler
    public UserGroupAggregate(UserGroupCreateCmd userGroupCreateCmd) {
        apply(new UserGroupCreatedEvent(userGroupCreateCmd.getId(), userGroupCreateCmd.getUserGroupData(), userGroupCreateCmd.getLoggedInUsername()));
    }


    @CommandHandler
    public void on(UserGroupUpdateCmd userGroupUpdateCmd){
        apply(new UserGroupUpdatedEvent(userGroupUpdateCmd.getId(), userGroupUpdateCmd.getUserGroupData(), userGroupUpdateCmd.getLoggedInUsername()));
    }


    @CommandHandler
    public void on(UserGroupDeleteCmd userGroupDeleteCmd){
        apply(new UserGroupDeletedEvent(userGroupDeleteCmd.getId(),userGroupDeleteCmd.getUserGroupData(),userGroupDeleteCmd.getLoggedInUsername()));
    }


    @EventSourcingHandler
    public void on(UserGroupCreatedEvent event) {
        id = event.getId();
        privileges = event.getUserGroupData().getPrivileges();
        ModelMapper modelMapper = new ModelMapper();
        userGroup = modelMapper.map(event.getUserGroupData(), UserGroup.class);
        isActive = true;
        loggedInUser = event.getLoggedInUsername();
    }


    @EventSourcingHandler
    public void on(UserGroupUpdatedEvent event){
        id = event.getId();
        privileges = event.getUserGroupData().getPrivileges();
        ModelMapper modelMapper = new ModelMapper();
        userGroup = modelMapper.map(event.getUserGroupData(), UserGroup.class);
        isActive = true;
        loggedInUser = event.getLoggedInUsername();

    }

    @EventSourcingHandler
    public void on(UserGroupDeletedEvent event){
        id = event.getId();
        isActive = false;
        loggedInUser = event.getLoggedInUsername();

    }


}
