package com.srini.userservice.domain.user.facade;

import com.srini.userservice.domain.user.model.Privilege;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupData {

    private Long user_group_id;
    private String id;
    private String name;
    private Privilege privileges;

}
