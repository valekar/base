package com.srini.userservice.domain.user.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Privilege {

    @Column(name="orders",nullable = false)
    private boolean orders;

    @Column(name = "inward" ,nullable = false,columnDefinition = "none")
    private boolean inward;

    @Column(name="production",nullable = false)
    private boolean production;

    @Column(name = "checking",nullable = false)
    private boolean checking;

    @Column(name="reports",nullable = false)
    private boolean reports;

    @Column(name = "admin", nullable = false,columnDefinition = "none")
    private boolean admin;

    @Column(name = "store",nullable = false)
    private boolean store;

    @Column(name="company",nullable = false)
    private boolean company;

    @Column(name="pre_production",nullable = false)
    private boolean preProduction;


}
