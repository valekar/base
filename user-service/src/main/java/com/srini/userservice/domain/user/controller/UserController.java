package com.srini.userservice.domain.user.controller;

import com.srini.userservice.domain.user.commands.UserCreateCmd;
import com.srini.userservice.domain.user.commands.UserDeleteCmd;
import com.srini.userservice.domain.user.commands.UserUpdateCmd;
import com.srini.userservice.domain.user.facade.UserData;
import com.srini.userservice.domain.user.facade.UserRequest;
import com.srini.userservice.domain.user.handlers.UserQueryHandler;
import com.srini.userservice.domain.user.model.User;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.common.IdentifierFactory;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Fetch the user data.
 */
@RestController
@RequestMapping("/user")
public class UserController {


    private UserQueryHandler userQueryHandler;
    private final CommandGateway commandGateway;
    private final IdentifierFactory identifierFactory = IdentifierFactory.getInstance();
    private EventStore eventStore;

    private static String EMAIL_TAKEN  = "Email already taken!";
    private static String USERNAME_TAKEN = "Username already taken!";
    private static String USER_DOES_NOT_EXISTS = "User not found";

    @Autowired
    public UserController(CommandGateway commandGateway, UserQueryHandler userQueryHandler, EventStore eventStore){
        this.userQueryHandler = userQueryHandler;
        this.commandGateway = commandGateway;
        this.eventStore = eventStore;
    }

    @GetMapping("{id}/events")
    public List<Object> getEvents(@PathVariable String id) {
        return eventStore.readEvents(id).asStream().map(s -> s.getPayload()).collect(Collectors.toList());
    }

    @GetMapping("/{username}")
    public User getUser(@PathVariable("username") String username){
        return userQueryHandler.findUserByUsername(username);
    }


    @PostMapping("/command/createUser")
    public ResponseEntity<String> createUser(@RequestBody UserRequest userRequest){

        if(userQueryHandler.findUserByUsername(userRequest.getUserData().getUsername()) == null) {
            if(userQueryHandler.findUserByEmail(userRequest.getUserData().getEmail()) == null){
                String eventId = commandGateway.sendAndWait(new UserCreateCmd(identifierFactory.generateIdentifier(),
                        userRequest.getUserData(), userRequest.getLoggedInUsername()));
                return new ResponseEntity<String>(eventId,HttpStatus.OK);
            }
            else{
                return new ResponseEntity<String>(EMAIL_TAKEN, HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>(USERNAME_TAKEN, HttpStatus.BAD_REQUEST);
    }


    @PostMapping("/command/updateUser")
    public ResponseEntity<String> updateUser(@RequestBody UserRequest userRequest){
        if(userQueryHandler.findUserById(userRequest.getUserData().getId()) != null){
            return commandGateway.sendAndWait(new UserUpdateCmd(userRequest.getUserData().getId(),
                    userRequest.getUserData(),userRequest.getLoggedInUsername()));
        }

        return new ResponseEntity<>(USER_DOES_NOT_EXISTS,HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/command/deleteUser")
    public ResponseEntity<String> deleteUser(@RequestBody UserRequest userRequest){
        // ideally you should not check here, you fire off appropriate commands in aggregate if it doesnt exist.
        if(userQueryHandler.findUserById(userRequest.getUserData().getId()) != null){
            return commandGateway.sendAndWait(new UserDeleteCmd(userRequest.getUserData().getId(),userRequest.getUserData(),userRequest.getLoggedInUsername()));
        }
        return new ResponseEntity<>(USER_DOES_NOT_EXISTS,HttpStatus.BAD_REQUEST);
    }
}
