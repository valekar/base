package com.srini.userservice.domain.user.controller;

import com.srini.userservice.domain.user.commands.UserGroupCreateCmd;
import com.srini.userservice.domain.user.commands.UserGroupDeleteCmd;
import com.srini.userservice.domain.user.commands.UserGroupUpdateCmd;
import com.srini.userservice.domain.user.facade.UserGroupRequest;
import com.srini.userservice.domain.user.handlers.UserGroupQueryHandler;
import com.srini.userservice.domain.user.model.UserGroup;
import org.apache.commons.lang.StringUtils;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.common.IdentifierFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/group")
public class UserGroupController {

    private UserGroupQueryHandler userGroupQueryHandler;
    private final CommandGateway commandGateway;
    private final IdentifierFactory identifierFactory = IdentifierFactory.getInstance();
    private static final String USER_GROUP_NOT_FOUND = "User Group does not exists!";
    private static final String USER_GROUP_ALREADY_EXISTS = "User Group already exists!";

    @Autowired
    public UserGroupController(CommandGateway commandGateway, UserGroupQueryHandler userGroupQueryHandler) {
        this.userGroupQueryHandler = userGroupQueryHandler;
        this.commandGateway = commandGateway;
    }


    @GetMapping("/command/{id}")
    public ResponseEntity<UserGroup> getUserGroupById(@PathVariable("id") String id) {
        UserGroup userGroup = userGroupQueryHandler.findUserGroupById(id);
        if (StringUtils.isNotEmpty(id) && userGroup != null) {
            return new ResponseEntity<>(userGroup, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PostMapping("/command/create")
    public ResponseEntity<String> createUserGroup(@RequestBody UserGroupRequest userGroupRequest) {
        if (userGroupQueryHandler.findUserGroupByName(userGroupRequest.getUserGroupData().getName()) == null) {
            String id = commandGateway.sendAndWait(new UserGroupCreateCmd(identifierFactory.generateIdentifier(),
                    userGroupRequest.getUserGroupData(), userGroupRequest.getLoggedInUsername()));
            return new ResponseEntity<>(id, HttpStatus.OK);
        }
        return new ResponseEntity<>(USER_GROUP_ALREADY_EXISTS, HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/command/update")
    public ResponseEntity<String> updateUserGroup(@RequestBody UserGroupRequest request) {
        if (userGroupQueryHandler.findUserGroupById(request.getUserGroupData().getId()) != null) {
            String id = commandGateway.sendAndWait(new UserGroupUpdateCmd(request.getUserGroupData().getId(),
                    request.getUserGroupData(), request.getLoggedInUsername()));
            return new ResponseEntity<>(id, HttpStatus.OK);
        }

        return new ResponseEntity<>(USER_GROUP_NOT_FOUND, HttpStatus.NOT_FOUND);
    }


    @PostMapping("/command/delete")
    public ResponseEntity<String> deleteUserGroup(@RequestBody UserGroupRequest request) {
        if (userGroupQueryHandler.findUserGroupById(request.getUserGroupData().getId()) != null) {
            String id = commandGateway.sendAndWait(new UserGroupDeleteCmd(request.getUserGroupData().getId(), request.getUserGroupData(),
                    request.getLoggedInUsername()));
            return new ResponseEntity<>(id, HttpStatus.OK);
        }
        return new ResponseEntity<>(USER_GROUP_NOT_FOUND, HttpStatus.NOT_FOUND);
    }
}
