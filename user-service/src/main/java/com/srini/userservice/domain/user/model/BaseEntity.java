/*
package com.srini.userservice.domain.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class BaseEntity {

    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_user_id")
    private Long created_user_id;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_user_id")
    private Long updated_user_id;

    @Column(name = "is_active")
    private boolean active = true;

  */
/*  public void setAddAuditFields(){
        LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();
        this.created_at = Utility.getTime();
        this.created_by = loggedInUser.getUserName();
        this.created_user_id = loggedInUser.getUserId();

        this.updated_at = Utility.getTime();
        this.updated_by = loggedInUser.getUserName();
        this.updated_user_id = loggedInUser.getUserId();
    }

    public void setEditAuditFields(){
        LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();
        this.updated_at = Utility.getTime();
        this.updated_by = loggedInUser.getUserName();
        this.updated_user_id = loggedInUser.getUserId();
    }
*//*



}
*/
