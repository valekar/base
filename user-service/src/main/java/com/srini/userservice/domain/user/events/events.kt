package com.srini.userservice.domain.user.events

import com.srini.userservice.domain.user.facade.UserData
import com.srini.userservice.domain.user.facade.UserGroupData
import com.srini.userservice.domain.user.model.User
import lombok.Value


data class UserCreatedEvent( val id:String, val userData: UserData,val loggedInUsername:String);
data class UserUpdatedEvent(val id:String, val userData: UserData,val loggedInUsername:String);
data class UserDeletedEvent(val id:String, val userData: UserData,val loggedInUsername:String);


data class UserGroupCreatedEvent(val id:String, val userGroupData: UserGroupData, val loggedInUsername: String);
data class UserGroupUpdatedEvent(val id:String, val userGroupData: UserGroupData, val loggedInUsername: String);
data class UserGroupDeletedEvent(val id:String, val userGroupData: UserGroupData, val loggedInUsername: String);