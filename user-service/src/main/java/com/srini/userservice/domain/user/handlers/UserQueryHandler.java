package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.model.User;
import com.srini.userservice.domain.user.repository.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserQueryHandler {



    private UserRepository userRepository;
    public UserQueryHandler(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User findUserByUsername(String username){
        User user = this.userRepository.findByUsername(username);
        return user;
    }


    public User findUserByEmailAndUsername(String email, String username){
        return this.userRepository.findByEmailAndUsername(email,username);
    }

    public User findUserByEmail(String email){
        return this.userRepository.findByEmail(email);
    }

    public User findUserById(String id){
        return this.userRepository.findById(id);
    }
}
