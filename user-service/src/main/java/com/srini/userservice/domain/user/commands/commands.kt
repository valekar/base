package com.srini.userservice.domain.user.commands

import com.srini.userservice.domain.user.facade.UserData
import com.srini.userservice.domain.user.facade.UserGroupData
import org.axonframework.modelling.command.TargetAggregateIdentifier
import javax.validation.constraints.NotNull


data class UserCreateCmd(@TargetAggregateIdentifier val id:String, @NotNull val userData: UserData, val loggedInUsername:String);
data class UserUpdateCmd(@TargetAggregateIdentifier val id:String, @NotNull val userData: UserData,val loggedInUsername:String);
data class UserDeleteCmd(@TargetAggregateIdentifier val id:String, @NotNull val userData: UserData,val loggedInUsername:String);

data class UserGroupCreateCmd(@TargetAggregateIdentifier val id:String, @NotNull val userGroupData: UserGroupData, val loggedInUsername: String);
data class UserGroupUpdateCmd(@TargetAggregateIdentifier val id:String, @NotNull val userGroupData: UserGroupData, val loggedInUsername: String);
data class UserGroupDeleteCmd(@TargetAggregateIdentifier val id:String,  @NotNull val userGroupData: UserGroupData,val loggedInUsername: String);

