package com.srini.userservice.domain.user.facade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupRequest {
    private UserGroupData userGroupData;
    private String loggedInUsername;
}
