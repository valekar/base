package com.srini.userservice.domain.user.handlers;

import com.srini.userservice.domain.user.events.UserGroupCreatedEvent;
import com.srini.userservice.domain.user.events.UserGroupDeletedEvent;
import com.srini.userservice.domain.user.events.UserGroupUpdatedEvent;
import com.srini.userservice.domain.user.model.User;
import com.srini.userservice.domain.user.model.UserGroup;
import com.srini.userservice.domain.user.repository.UserGroupRepository;
import com.srini.userservice.domain.user.repository.UserRepository;
import org.axonframework.eventhandling.EventHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.logging.Logger;

@Component
public class UserGroupEventHandler extends BaseHandler{

    private  UserGroupRepository userGroupRepository;
    private  ModelMapper modelMapper;
    private UserRepository userRepository;

    private final static Logger LOGGER = Logger.getLogger(UserEventHandler.class.getName());


    public UserGroupEventHandler(UserGroupRepository userGroupRepository, UserRepository userRepository){
        this.modelMapper = new ModelMapper();
        this.userGroupRepository = userGroupRepository;
        this.userRepository = userRepository;
    }


    @EventHandler
    public void on(UserGroupCreatedEvent event){
        LOGGER.info("User Group Created Event " + event);
        User loggedInUser = userRepository.findByUsername(event.getLoggedInUsername());
        UserGroup userGroup = modelMapper.map(event.getUserGroupData(), UserGroup.class);
        userGroup.setId(event.getId());
        userGroup.setActive(true);
        userGroup.setCreated_at(new Timestamp(System.currentTimeMillis()));
        userGroup.setCreated_by(event.getLoggedInUsername());
        userGroup.setUpdated_by(event.getLoggedInUsername());
        userGroup.setCreated_user_id(loggedInUser.getId());
        userGroup.setUpdated_user_id(loggedInUser.getId());
        userGroup.setUpdated_at(new Timestamp(System.currentTimeMillis()));
        userGroupRepository.save(userGroup);
    }

    @EventHandler
    public void on(UserGroupUpdatedEvent event){
        LOGGER.info("User Group Updated Event " + event);
        UserGroup userGroup = userGroupRepository.findUserGroupById(event.getId());

        if(userGroup!=null){
            UserGroup saveUserGroup = modelMapper.map(event.getUserGroupData(),UserGroup.class);
            User loggedInUser = userRepository.findByUsername(event.getLoggedInUsername());
            saveUserGroup.setUser_group_id(userGroup.getUser_group_id());
            saveUserGroup.setActive(true);
            saveUserGroup.setCreated_user_id(userGroup.getCreated_user_id());
            saveUserGroup.setCreated_by(userGroup.getCreated_by());
            saveUserGroup.setCreated_at(userGroup.getCreated_at());
            if(loggedInUser!=null){

                saveUserGroup = setAudits(loggedInUser,saveUserGroup);
            }

            userGroupRepository.save(saveUserGroup);
        }
    }


    @EventHandler
    public void on(UserGroupDeletedEvent userGroupDeletedEvent){
        LOGGER.info("User Group Deleted Event " + userGroupDeletedEvent);
        UserGroup userGroup= userGroupRepository.findUserGroupById(userGroupDeletedEvent.getId());
        if(userGroup!=null){
            userGroup.setActive(false);

            User loggedInUser = userRepository.findByUsername(userGroupDeletedEvent.getLoggedInUsername());
            if(loggedInUser!=null){
                userGroup = setAudits(loggedInUser,userGroup);
            }
            userGroupRepository.save(userGroup);
        }
    }

}
