package com.srini.userservice.domain.user.model;


import lombok.*;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "users")
@Where(clause = "is_active = true")
@Getter
@Setter
@NoArgsConstructor
public class User {

    static final long serialVersionUID = 1L;


    public User(String username){
        this.username = username;
    }



    @javax.persistence.Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false, updatable = false)
    private Long userId;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

   @Column(name = "id")
    private String id;


    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "is_active", nullable = false)
    private boolean active = true;

    @Embedded
    private UserDetails userDetails;


    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_user_id")
    private String created_user_id;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_user_id")
    private String updated_user_id;




    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "users_user_groups",
            joinColumns =
            @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
            inverseJoinColumns =
            @JoinColumn(name = "user_group_id", referencedColumnName = "user_group_id"))
    private UserGroup userGroup;

  /*  public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
*/

    public boolean isAccountNonExpired() {
        return true;
    }


    public boolean isAccountNonLocked() {
        // we never lock accounts
        return true;
    }


    public boolean isCredentialsNonExpired() {
        // credentials never expire
        return true;
    }



}