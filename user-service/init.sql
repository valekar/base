CREATE TABLE user_groups
(
    user_group_id   bigserial primary key,
    id              varchar(255) unique,
    name            VARCHAR(255),
    orders          BOOL      default false,
    inward          BOOL      default false,
    pre_production  BOOL      default false,
    production      BOOL      default false,
    checking        BOOL      default false,
    reports         BOOL      default false,
    admin           BOOL      default false,
    store           BOOL      default false,
    company         BOOL      default false,
    created_at      timestamp default CURRENT_TIMESTAMP,
    updated_at      timestamp default CURRENT_TIMESTAMP,
    created_by      varchar(40)            not null,
    created_user_id varchar(255)           not null,
    updated_by      varchar(40)            not null,
    updated_user_id varchar(255)           not null,
    is_active       BOOL      default true not null
);

CREATE TABLE users
(
    user_id         bigserial primary key,
    id              varchar(256) UNIQUE,
    username        VARCHAR(128),
    password        VARCHAR(256),
    enabled         BOOL,
    email           varchar(255),
    line_1          varchar(256),
    line_2          varchar(256),
    pin_code        varchar(20),
    city            varchar(256),
    state           varchar(256),
    country         varchar(256),
    county          varchar(256),
    age             BIGINT,
    sex             varchar(20),
    blood_group     varchar(20),
    created_at      timestamp default CURRENT_TIMESTAMP,
    updated_at      timestamp default CURRENT_TIMESTAMP,
    created_by      varchar(255)           not null,
    created_user_id varchar(255)           not null,
    updated_by      varchar(40)            not null,
    updated_user_id varchar(255)           not null,
    is_active       BOOL      default true not null
);



CREATE TABLE users_user_groups
(
    user_id       BIGINT references users (user_id),
    user_group_id BIGINT references user_groups (user_group_id)
);



INSERT INTO users (id, user_id, username, password, enabled, email, created_by, created_user_id, updated_by,
                   updated_user_id, is_active)
VALUES ('1', 1, 'srini', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true,
        'srinivas.valekar@gmail.com', 'srini', 0, 'srini', 0, true),
       ('2', 2, 'kanishka', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true,
        'kanishkasrijith@gmail.com', 'srini', 0, 'srini', 0, true),
       ('3', 3, 'srijith', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true,
        'spcreation.emb@gmail.com', 'srini', 0, 'srini', 0, true);


/*create table password_reset_tokens(
    token_id Bigserial primary key,
    token varchar(255),
    user_id bigint,
    expiry_date timestamp default CURRENT_TIMESTAMP + INTERVAL '1 day'
);*/

